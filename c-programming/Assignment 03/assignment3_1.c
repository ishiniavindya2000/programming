#include <stdio.h>

int main()
{
    int x = 0;
    
    printf("Enter an integer you like\n");
    scanf("%d", &x);
    
    if (x > 0) {
        printf("The number you entered is Positive");
    }
    
    if (x < 0) {
        printf("The number you entered is Negative");
    }
    
    if (x == 0) {
        printf("The number you entered is Zero");
    }
return 0;

}
