 #include <stdio.h>
  int main () {
        int a, b, num;
        printf("Enter a number to print multiplication tables\n");
        scanf("%d", &num);

        for (a = 1; a <= num; a++) {
                printf("Multiplicaton table for %d is\n", a);
                for (b = 1; b <= 12; b++) {
                        
                        printf("%2d X %2d = %3d\n", a, b, a * b);
                }
                printf("\n");
        }
        return 0;
  }
 
