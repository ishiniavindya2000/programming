#include<stdio.h>

#define costPerPerformance 500
#define costPerPerson 3

int numOFpeople(int price);
int cost(int price);
int profit(int price);
int income(int price);

int numOFpeople(int price){
return 120 - ((price - 15)/5)*20;
}

int cost(int price){
return numOFpeople(price) * costPerPerson + costPerPerformance;
}

int profit(int price){
return income(price) - cost(price);
}

int income(int price){
return numOFpeople(price) * price;
}

int main(){
int price;
printf("Estimated profit for the ticket prices are:\n");
printf("Note that profits with minus mark denote that the particular ticket price is not profitable at all.\n");
for(price = 5; price < 50; price+=5)
{
printf("Ticket price = Rs%d\n Profit = Rs%d\n\n", price,profit(price));
}

}
