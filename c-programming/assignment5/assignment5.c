#include<stdio.h>

int cost(int num_people);

int revenue(int price, int num_people);

int main()
{
int num_people, price, profit1, profit2, profit3;




printf("The first method is:\n price of ticket = Rs.15\n number of attendants = 120\n"); 
profit1 = revenue(15, 120) - cost(120);
printf("The first method brings you a profit of Rs.%d\n", profit1);


printf("The second method is:\n price of ticket = Rs.10\n number of attendants = 140\n");
profit2 = revenue(10, 140) - cost(140);
printf("The second method brings you a profit of Rs.%d\n", profit2);


printf("The third method is:\n price of ticket = Rs.20\n number of attendants = 100\n");
profit3 = revenue(20, 100) - cost(100);
printf("The third method brings you a profit of Rs.%d\n", profit3);


if (profit1 > profit2 && profit1 > profit3)
   {
   printf("The best method to go is the first one: which gives you a profit of Rs.%d\n", profit1);
}

if (profit2 > profit1 && profit2 > profit3) {
  printf("The best method to go is the second one: which gives you a profit of Rs.%d\n", profit2);
}

if (profit3 > profit1 && profit3 > profit2) {
printf("The best method to go is the third one: which gives you a profit of Rs.%d\n", profit3);
}

return 0;
}

int cost(int num_people){
 return 500 + (3 * num_people);
}


int revenue(int price, int num_people){
return price * num_people;
}


