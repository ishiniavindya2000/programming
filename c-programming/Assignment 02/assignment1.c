#include <stdio.h>

int main(void)
{
    float r, area;
    const float pi = 3.14;
    printf("Enter the radius of a disk: \n");
    scanf("%f", &r);
    area = pi * r * r;
    printf("The area of the disk is: %f\n", area);

    return 0;
}
