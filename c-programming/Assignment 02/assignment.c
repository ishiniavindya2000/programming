#include <stdio.h>

int main(void)
{
	float x;
	float y;
	float z;
	printf("Enter a floating number you like: ");
	scanf("%f", &x );
	printf("Enter another floating number you like:  ");
	scanf("%f", &y);

	z = x * y;

	printf("The product of the numbers you entered is: %.2f\n", z);
	return 0;
}
